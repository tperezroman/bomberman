package sistemajuegos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;



public class SistemaJuegos extends JFrame implements ActionListener, ItemListener{
    private  int ancho = 800;
    private  int alto = 600;
    //menubar
    private JMenuBar barraMenu;
    private JMenu file;
    private JMenu view;
    private JMenu options;
    private JMenu help;
    //lista de juegos
    private JPanel panelListaJuegos;
    private List listaJuegos;
    private JList jList;
    private String arrayDescrip[] = {"Bomber","CSGO","LoL"};
    //imagen
    private JPanel panelImagen;
    private MyCanvas canvas;
    private String arrayRuta[] = {"src/imagenes/plataforma/Bomberman.jpg","src/imagenes/plataforma/CsGO.jpg","src/imagenes/plataforma/LoL.jpg"};
    private Image arrayImagenes[] = new Image[4];
    //boton play y panel inferior
    private JButton play;
    private JPanel panelBotones;
    private MenuInicio presentacion;
    
    public SistemaJuegos (){
        
        this.setLayout(new BorderLayout());
        //MenuBar
        barraMenu = new JMenuBar();
        file = new JMenu("File");
        view = new JMenu("View");
        options = new JMenu("Options");
        help = new JMenu("Help");
        barraMenu.add(file);
        barraMenu.add(view);
        barraMenu.add(options);
        barraMenu.add(help);
        this.add(barraMenu,BorderLayout.NORTH);
        //lista de juegos
        panelListaJuegos = new JPanel();
        panelListaJuegos.setLayout(new BorderLayout());
        listaJuegos = new List();
        listaJuegos.addItemListener(this);
        listaJuegos.add("Bomberman");
        listaJuegos.add("Counter Strike Global Offensive");
        listaJuegos.add("League of Legends");
        listaJuegos.setForeground(Color.BLACK);
        listaJuegos.select(0);
        
        panelListaJuegos.add(listaJuegos, BorderLayout.CENTER);
        this.add(panelListaJuegos,BorderLayout.WEST);
        //panel que muestra las imagenes
        panelImagen = new JPanel();
        panelImagen.setLayout(new BorderLayout());
        
        for(int i=0;i<arrayRuta.length;i++){
            try{
		arrayImagenes[i] = ImageIO.read(new File(arrayRuta[i]));
		arrayImagenes[i] = arrayImagenes[i].getScaledInstance(800,400,Image.SCALE_SMOOTH);
            }
            catch (IOException e){
                System.out.println("Error al cargar la imagen.");
            }        
        }
        canvas = new MyCanvas(arrayImagenes[0]);
        //agrego la imagen al panel y luego el panel al frame
        panelImagen.add(canvas,BorderLayout.CENTER);
        this.add(panelImagen,BorderLayout.CENTER);
        //Agrego panel y boton de PLAY
        panelBotones = new JPanel();
        play = new JButton("Play");
        play.addActionListener(this);
        panelBotones.add(play);
        this.add(panelBotones,BorderLayout.SOUTH);
        //preferencias del JFrame
        this.setVisible(true);
        this.setSize(ancho,alto);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.pack();
        this.getRootPane().setDefaultButton(play);//para seleccionar el juego con la tecla enter
        listaJuegos.requestFocus();
        
    }
    
    public static void main (String[] args){
        SistemaJuegos p = new SistemaJuegos();
        p.revalidate();
    }

    public void actionPerformed(ActionEvent e) {
        Object botonPulsado = e.getSource();
        
        if(botonPulsado == play && listaJuegos.getSelectedItem() == "Bomberman"){
            System.out.println("Abriendo Bomberman..");
            this.dispose();
            presentacion=new MenuInicio();

        }
        if(botonPulsado == play && listaJuegos.getSelectedItem() == "Counter Strike Global Offensive"){
            System.out.println("Abriendo Cs GO..");
        }
        if(botonPulsado == play && listaJuegos.getSelectedItem() == "League of Legends"){
            System.out.println("Abriendo LoL..");
        }
    }
    
    
    public void itemStateChanged(ItemEvent e) {
        canvas.cambiarImagen(arrayImagenes[listaJuegos.getSelectedIndex()]);
    }
}