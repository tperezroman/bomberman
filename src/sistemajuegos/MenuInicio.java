package sistemajuegos;

import bomberman.Bomberman;
import bomberman.FXPlayer;
import bomberman.Ranking;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.*;

public class MenuInicio extends JPanel implements ActionListener {
	private JButton boton_play;
        private JButton boton_top;
        private JButton boton_exit;
        private JButton boton_settings;
	private JLabel title;
	private Bomberman juego;
	private final JFrame frame;
	private final Image fondo;

	public MenuInicio() {
		this.setLayout(null);
                this.setPreferredSize(new Dimension(800,100));
		initComponents();
		fondo = new ImageIcon("src/imagenes/plataforma/start.png").getImage();
		frame = new JFrame();
		frame.add(this);
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
                this.getRootPane().setDefaultButton(boton_play);//para seleccionar el juego con la tecla enter
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(fondo, 0, 0, getWidth(), getHeight(), this);
                if(!FXPlayer.GAMEOVER.isRunning()){
                    FXPlayer.MENU.play();
                }
	}

	private void initComponents() {
                boton_play = new JButton("PLAY");
                boton_play.setForeground(Color.WHITE);
                boton_play.setBackground(Color.black);
                boton_play.setBorder(null);
                boton_play.setFocusable(false);
                boton_play.addActionListener(this);
                boton_play.setFont(new Font("Helvetica", Font.BOLD, 24));
                this.add(boton_play);
                boton_play.setLocation(215, 360);
                boton_play.setSize(125,30);
                
                boton_top = new JButton("TOP");
                boton_top.setForeground(Color.WHITE);
                boton_top.setBackground(Color.black);
                boton_top.setBorder(null);
                boton_top.setFocusable(false);
                boton_top.addActionListener(this);
                boton_top.setFont(new Font("Helvetica", Font.BOLD, 24));
                this.add(boton_top);
                boton_top.setLocation(464, 400);
                boton_top.setSize(100,30);
                
                boton_exit = new JButton("EXIT");
                boton_exit.setForeground(Color.WHITE);
                boton_exit.setBackground(Color.black);
                boton_exit.setBorder(null);
                boton_exit.setFocusable(false);
                boton_exit.addActionListener(this);
                boton_exit.setFont(new Font("Helvetica", Font.BOLD, 24));
                this.add(boton_exit);
                boton_exit.setLocation(450, 360);
                boton_exit.setSize(130,30);
                
                boton_settings = new JButton("MULTIPLAYER");
                boton_settings.setForeground(Color.WHITE);
                boton_settings.setBackground(Color.black);
                boton_settings.setBorder(null);
                boton_settings.setFocusable(false);
                boton_settings.addActionListener(this);
                boton_settings.setFont(new Font("Helvetica", Font.BOLD, 24));
                this.add(boton_settings);
                boton_settings.setLocation(190, 400);
                boton_settings.setSize(180,30);
	}

	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
                    case "PLAY": {
                        frame.dispose();
                        juego = new Bomberman(false);
                        juego.start();
                        FXPlayer.MENU.stop();
                        break;
                    }
                    case "MULTIPLAYER": {
                        frame.dispose();
                        juego = new Bomberman(true);
                        juego.start();
                        FXPlayer.MENU.stop();
                        break;
                    }
                    case "TOP": {
                        JDialog jd = new JDialog();
			jd.setModal(true);
			jd.setLayout(new BorderLayout());
			Ranking ranking = new Ranking();
			jd.add(ranking);
			jd.setSize(800, 600);
			jd.setLocationRelativeTo(null);
			jd.setVisible(true);
			break;
                    }
                    case "EXIT": {
                            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
                            break;
                    }
		}
	}

}
