package sistemajuegos;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

public class MyCanvas extends Canvas{
    private Image imagen;

    public MyCanvas(Image i){
	imagen = i;
	this.setPreferredSize(new Dimension(800,400));
    }

    public void cambiarImagen(Image i){
	imagen = i;
	repaint();
    }

    @Override
    public void paint(Graphics g){
	g.setColor(Color.BLACK);
	g.fillRect(0,0,getWidth(),getHeight());
	g.drawRect(0,0,imagen.getWidth(this)+1 ,imagen.getHeight(this)+1);
	g.drawImage(imagen,0,0,this);
    }

}