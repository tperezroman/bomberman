package sprite;
import sprite.HojaSprites;
import java.awt.image.BufferedImage;

public class Sprite {
	private final int lado;//tamanio del sprite(lado x lado al ser un cuadrado)
        private int x;
        private int y;
        private HojaSprites hoja;
        public int[] pixeles;
        
        //coleccion de sprites del mapa
        public static Sprite VACIO = new Sprite(32,0);//0 representa el color negro
        public static Sprite PAREDSOLIDA_SPRITE = new Sprite(32, 0, 0, HojaSprites.HOJA_PAREDES);
        public static Sprite SUELO_SPRITE = new Sprite(32, 8, 0, HojaSprites.HOJA_PAREDES);//tam,x,y,dedonde
        
        //Coleccion de pared rompible
	public static Sprite PAREDROMPIBLE0_SPRITE = new Sprite(32, 1, 0, HojaSprites.HOJA_PAREDES);
        public static Sprite PAREDROMPIBLE1_SPRITE = new Sprite(32, 2, 0, HojaSprites.HOJA_PAREDES);
        public static Sprite PAREDROMPIBLE2_SPRITE = new Sprite(32, 3, 0, HojaSprites.HOJA_PAREDES);
        public static Sprite PAREDROMPIBLE3_SPRITE = new Sprite(32, 4, 0, HojaSprites.HOJA_PAREDES);
        public static Sprite PAREDROMPIBLE4_SPRITE = new Sprite(32, 5, 0, HojaSprites.HOJA_PAREDES);
        public static Sprite PAREDROMPIBLE5_SPRITE = new Sprite(32, 6, 0, HojaSprites.HOJA_PAREDES);
        public static Sprite PAREDROMPIBLE6_SPRITE = new Sprite(32, 7, 0, HojaSprites.HOJA_PAREDES);
        public static Sprite PUERTA_SPRITE = new Sprite(32, 0, 1, HojaSprites.HOJA_PAREDES);
	
        
        //coleccion de sprites del heroe
        public static Sprite HEROE_ARRIBA1_SPRITE = new Sprite(32, 0, 1, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_ARRIBA2_SPRITE = new Sprite(32, 1, 1, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_ARRIBA3_SPRITE = new Sprite(32, 2, 1, HojaSprites.HOJA_HEROE);
        
        public static Sprite HEROE_DERECHA1_SPRITE = new Sprite(32, 0, 2, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_DERECHA2_SPRITE = new Sprite(32, 1, 2, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_DERECHA3_SPRITE = new Sprite(32, 2, 2, HojaSprites.HOJA_HEROE);
        
        public static Sprite HEROE_ABAJO1_SPRITE = new Sprite(32, 0, 3, HojaSprites.HOJA_HEROE);//CUANDO ESTA QUIETO
        public static Sprite HEROE_ABAJO2_SPRITE = new Sprite(32, 1, 3, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_ABAJO3_SPRITE = new Sprite(32, 2, 3, HojaSprites.HOJA_HEROE);
        
        public static Sprite HEROE_IZQUIERDA1_SPRITE = new Sprite(32, 0, 4, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_IZQUIERDA2_SPRITE = new Sprite(32, 1, 4, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_IZQUIERDA3_SPRITE = new Sprite(32, 2, 4, HojaSprites.HOJA_HEROE);
        
        public static Sprite HEROE_MUERTE1_SPRITE = new Sprite(32, 0, 5, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_MUERTE2_SPRITE = new Sprite(32, 1, 5, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_MUERTE3_SPRITE = new Sprite(32, 2, 5, HojaSprites.HOJA_HEROE);
        public static Sprite HEROE_MUERTE4_SPRITE = new Sprite(32, 3, 5, HojaSprites.HOJA_HEROE);
        
         //coleccion de sprites del heroe2
        public static Sprite HEROE2_ARRIBA1_SPRITE = new Sprite(32, 0, 1, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_ARRIBA2_SPRITE = new Sprite(32, 1, 1, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_ARRIBA3_SPRITE = new Sprite(32, 2, 1, HojaSprites.HOJA_HEROE2);
        
        public static Sprite HEROE2_DERECHA1_SPRITE = new Sprite(32, 0, 2, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_DERECHA2_SPRITE = new Sprite(32, 1, 2, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_DERECHA3_SPRITE = new Sprite(32, 2, 2, HojaSprites.HOJA_HEROE2);
        
        public static Sprite HEROE2_ABAJO1_SPRITE = new Sprite(32, 0, 3, HojaSprites.HOJA_HEROE2);//CUANDO ESTA QUIETO
        public static Sprite HEROE2_ABAJO2_SPRITE = new Sprite(32, 1, 3, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_ABAJO3_SPRITE = new Sprite(32, 2, 3, HojaSprites.HOJA_HEROE2);
        
        public static Sprite HEROE2_IZQUIERDA1_SPRITE = new Sprite(32, 0, 4, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_IZQUIERDA2_SPRITE = new Sprite(32, 1, 4, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_IZQUIERDA3_SPRITE = new Sprite(32, 2, 4, HojaSprites.HOJA_HEROE2);
        
        public static Sprite HEROE2_MUERTE1_SPRITE = new Sprite(32, 0, 5, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_MUERTE2_SPRITE = new Sprite(32, 1, 5, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_MUERTE3_SPRITE = new Sprite(32, 2, 5, HojaSprites.HOJA_HEROE2);
        public static Sprite HEROE2_MUERTE4_SPRITE = new Sprite(32, 3, 5, HojaSprites.HOJA_HEROE2);
        
        public static Sprite FANTASMA1_MOVIMIENTO1_SPRITE = new Sprite(32, 0, 0, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MOVIMIENTO2_SPRITE = new Sprite(32, 1, 0, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MOVIMIENTO3_SPRITE = new Sprite(32, 2, 0, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MOVIMIENTO4_SPRITE = new Sprite(32, 3, 0, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MOVIMIENTO5_SPRITE = new Sprite(32, 4, 0, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MOVIMIENTO6_SPRITE = new Sprite(32, 5, 0, HojaSprites.HOJA_FANTASMAS);
        
        public static Sprite FANTASMA1_MUERTE1_SPRITE = new Sprite(32, 0, 1, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MUERTE2_SPRITE = new Sprite(32, 1, 1, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MUERTE3_SPRITE = new Sprite(32, 2, 1, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MUERTE4_SPRITE = new Sprite(32, 3, 1, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MUERTE5_SPRITE = new Sprite(32, 4, 1, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA1_MUERTE6_SPRITE = new Sprite(32, 5, 1, HojaSprites.HOJA_FANTASMAS);
        
        public static Sprite FANTASMA2_MOVIMIENTO1_SPRITE = new Sprite(32, 0, 2, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA2_MOVIMIENTO2_SPRITE = new Sprite(32, 1, 2, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA2_MOVIMIENTO3_SPRITE = new Sprite(32, 2, 2, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA2_MOVIMIENTO4_SPRITE = new Sprite(32, 3, 2, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA2_MOVIMIENTO5_SPRITE = new Sprite(32, 4, 2, HojaSprites.HOJA_FANTASMAS);
        public static Sprite FANTASMA2_MOVIMIENTO6_SPRITE = new Sprite(32, 5, 2, HojaSprites.HOJA_FANTASMAS);
        
        public static Sprite FANTASMA2_MUERTE_SPRITE = new Sprite(32, 0, 3, HojaSprites.HOJA_FANTASMAS);
        //coleccion de sprites de los bonus
        public static Sprite BONUS_BOMBA_EXTRA = new Sprite(32, 1, 0, HojaSprites.HOJA_BONUS);
        public static Sprite BONUS_DETONADOR = new Sprite(32, 4, 0, HojaSprites.HOJA_BONUS);
        public static Sprite BONUS_RANGO_EXPLOSION = new Sprite(32, 0, 0, HojaSprites.HOJA_BONUS);
        public static Sprite BONUS_VIDA = new Sprite(32, 3, 0, HojaSprites.HOJA_BONUS);
        public static Sprite BONUS_VELOCIDAD = new Sprite(32, 2, 0, HojaSprites.HOJA_BONUS);
        public static Sprite BONUS_SALTO_BOMBA = new Sprite(32, 5, 0, HojaSprites.HOJA_BONUS);
        
        
        //coleccion de sprites de la bomba
        public static Sprite BOMBA1_SPRITE = new Sprite(32, 0, 0, HojaSprites.HOJA_BOMBAS);
        public static Sprite BOMBA2_SPRITE = new Sprite(32, 1, 0, HojaSprites.HOJA_BOMBAS);
        public static Sprite BOMBA3_SPRITE = new Sprite(32, 2, 0, HojaSprites.HOJA_BOMBAS);
        //coleccion de sprites de la explosion y alargue bonus
        public static Sprite EXPLOSION_CENTRO_SPRITE = new Sprite(32, 1, 2, HojaSprites.HOJA_BOMBAS);
        public static Sprite EXPLOSION_ARRIBA_SPRITE = new Sprite(32, 1, 1, HojaSprites.HOJA_BOMBAS);
        public static Sprite EXPLOSION_ABAJO_SPRITE = new Sprite(32, 1, 3, HojaSprites.HOJA_BOMBAS);
        public static Sprite EXPLOSION_IZQUIERDA_SPRITE = new Sprite(32, 0, 2, HojaSprites.HOJA_BOMBAS);
        public static Sprite EXPLOSION_DERECHA_SPRITE = new Sprite(32, 2, 2, HojaSprites.HOJA_BOMBAS);
        
        public static Sprite EXPLOSION_ALARGUE_VERTICAL = new Sprite(32, 3, 1, HojaSprites.HOJA_BOMBAS);
        public static Sprite EXPLOSION_ALARGUE_HORIZONTAL = new Sprite(32, 3, 3, HojaSprites.HOJA_BOMBAS);
        
	public Sprite(final int lado, final int columna, final int fila, final HojaSprites hoja){
		this.lado = lado;
                pixeles= new int[this.lado*this.lado];

		this.x=columna * lado;
                this.y=fila * lado;
                this.hoja=hoja;
                
                for(int y=0; y<lado; y++){//recorre los pixeles del sprite que sacamos de la HojaSprites
                    for(int x=0; x<lado; x++){
                        pixeles[x+y*lado]= hoja.pixeles[(x+this.x)+(y+this.y)*hoja.getAncho()];
                    }
                }
	}
        
        //otro constructor
        public Sprite(final int lado, final int color){
            this.lado = lado;
            pixeles = new int[this.lado * this.lado];
            for(int i=0; i<pixeles.length; i++){
                pixeles[i] = color;
            }
        }

	public int obtenerAncho() {
		return lado;
	}
}
