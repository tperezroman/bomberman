package sprite;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

public class HojaSprites {

	private final int ancho;
	private final int alto;
        private final String ruta;
        //coleccion de hoja de sprites
        public static HojaSprites HOJA_PAREDES = new HojaSprites("/imagenes/sprites/SpriteSheet.png",320,320);
        public static HojaSprites HOJA_HEROE = new HojaSprites("/imagenes/sprites/Heroe.png",320,320);
        public static HojaSprites HOJA_HEROE2 = new HojaSprites("/imagenes/sprites/Heroe2.png",320,320);
        public static HojaSprites HOJA_FANTASMAS = new HojaSprites("/imagenes/sprites/Fantasmas.png",320,320);
        public static HojaSprites HOJA_BOMBAS = new HojaSprites("/imagenes/sprites/Bomba.png",320,320);
        public static HojaSprites HOJA_BONUS = new HojaSprites("/imagenes/sprites/Bonus.png",320,320);
        
        public final int[] pixeles;
        
        public HojaSprites(final String ruta, final int ancho,final int alto){
            this.ancho=ancho;
            this.alto=alto;
            this.ruta=ruta;
            pixeles = new int[ancho*alto];
            
            try{
                BufferedImage imagen = ImageIO.read(HojaSprites.class.getResource(ruta));
                imagen.getRGB(0, 0, ancho, alto,pixeles, 0, ancho);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        
        public int getAncho(){
            return ancho;
        }
}
