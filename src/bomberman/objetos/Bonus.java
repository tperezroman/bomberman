package bomberman.objetos;

import bomberman.objetoGrafico.ObjetoGrafico;
import bomberman.Mapa;
import bomberman.Tile;
import java.util.Random;

public class Bonus extends ObjetoGrafico{
    private Random random;
    private final int fantasmasBonus=6;
    private final Tile[] tipo_bonus = {Tile.BONUS_BOMBA_EXTRA,Tile.BONUS_DETONADOR,Tile.BONUS_RANGO,Tile.BONUS_SALTO,Tile.BONUS_VELOCIDAD,Tile.BONUS_VIDA};
    
    public Bonus(int x, int y,int posicion, Mapa mapa) {
            setX(x);
            setY(y);
            random = new Random();
            this.mapa = mapa;
            addBonus(posicion);
	}
    
    public void addBonus(int posicion) {
        int num=0;
        if(mapa.getPuerta()  && mapa.getTemporizador()<80){
            mapa.addPuerta(posicion);
        }
        else{
            num=random.nextInt(80);
            if(addFantasma(posicion)){
                mapa.tilesCatalogo[posicion] = Tile.SUELO;    
            }
            else if(mapa.getTilesCatalogo(posicion)==Tile.PUERTA){
                mapa.tilesCatalogo[posicion] = Tile.PUERTA;
                mapa.addFantasmaColision(fantasmasBonus, getX(), getY());
            }
            else{
                switch (num) {
                case 0:
                    mapa.tilesCatalogo[posicion] = Tile.BONUS_VIDA;
                    break;       
                case 1:
                    mapa.tilesCatalogo[posicion] = Tile.BONUS_BOMBA_EXTRA;
                    break;
                case 7:
                    mapa.tilesCatalogo[posicion] = Tile.BONUS_DETONADOR;
                    break;
                case 13:
                    mapa.tilesCatalogo[posicion] = Tile.BONUS_RANGO;
                    break;
                case 19:
                    mapa.tilesCatalogo[posicion] = Tile.BONUS_SALTO;
                    break;
                case 25:
                    mapa.tilesCatalogo[posicion] = Tile.BONUS_VELOCIDAD;
                    break;
                case 31:
                    if(mapa.getPuerta()){
                        mapa.setPuerta(false);
                        mapa.tilesCatalogo[posicion] = Tile.PUERTA;
                        break;
                    }
                default:
                    mapa.tilesCatalogo[posicion] = Tile.SUELO;
                    break;    
                }
            }
        }
        
             
    }
    
    public boolean addFantasma(int posicion){
        for(int i=0; i<tipo_bonus.length; i++){
            if(mapa.getTilesCatalogo(posicion)==tipo_bonus[i]){
                mapa.addFantasmaColision(fantasmasBonus, getX(), getY());
                return true;
            }
        }
        return false;
        
    }

    public boolean colision(int dx, int dy) {
        return false;
    }
    
}