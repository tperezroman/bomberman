package bomberman.objetos;

import bomberman.Pantalla;
import bomberman.FXPlayer;
import bomberman.Mapa;
import bomberman.Teclado;
import bomberman.Tile;
import bomberman.entes.Heroe;
import bomberman.objetoGrafico.ObjetoGrafico;
import sprite.Sprite;


public class Bomba extends ObjetoGrafico{
    private int animacion;
    private int bombCounter;
    private int rango;
    private Heroe heroe;
    private Teclado teclado;
    boolean solid = true;

	
    public Bomba(int x, int y, Sprite sprite, Mapa mapa, int range, Heroe heroe, Teclado teclado) {
            setX(x);
            setY(y);
            this.heroe = heroe;
            this.teclado = teclado;
            this.sprite = sprite;
            this.rango = range;
            animacion = 0;
            bombCounter = 0;
            this.mapa = mapa;
    }

    public void actualizar(Pantalla pantalla) {
            if(animacion < 7500) animacion++;
            else animacion = 0;
            
            if(!heroe.getInterruptor()){
                if(bombCounter > 40 || colisionBombas()) {
                    mapa.llama.add(new Llama(getX(), getY(), rango, Sprite.EXPLOSION_CENTRO_SPRITE, mapa));
                    //la bomba a remover siempre va a ser la primera, por eso el 0 en el get()
                    //y vuelvo a poner el Tile.SUELO donde estaba la bomba
                    mapa.tilesCatalogo[(mapa.bomba.get(0).getX()>>5)+((mapa.bomba.get(0).getY()>>5))*25] = Tile.SUELO;
                    FXPlayer.EXPLOSION.play();
                    eliminar();  
                }
            }
            else if(heroe.getInterruptor() && teclado.interruptor){
                mapa.llama.add(new Llama(getX(), getY(), rango, Sprite.EXPLOSION_CENTRO_SPRITE, mapa));
                FXPlayer.EXPLOSION.play();
                eliminar();   
            }

            if(animacion % 30 > 20) {
                    sprite = Sprite.BOMBA1_SPRITE;
            }else if(animacion % 30 > 10) {
                sprite = Sprite.BOMBA2_SPRITE;
            }else {
                    sprite = Sprite.BOMBA3_SPRITE;
                    bombCounter++;
            }

            pantalla.mostrarBomba(getX(), getY(), this);
    }
    
    public boolean colisionBombas(){
        for(int i = 0; i < mapa.llama.size(); i++) {
            for(int r=1; r<=rango; r++){
                if((getX()+30>>5 == mapa.llama.get(i).getX() >> 5 || getX()>>5 == mapa.llama.get(i).getX() >> 5 ) && (getY()+25>>5 == mapa.llama.get(i).getY() >> 5 || getY()>>5 == mapa.llama.get(i).getY() >> 5)) {
                    return true;
                }

                if(getX()+5>>5 == (mapa.llama.get(i).getX() >> 5) + r && (getY()+25>>5 == mapa.llama.get(i).getY()>>5 || getY() >> 5 ==  mapa.llama.get(i).getY()>>5)) {
                    return true;
                }

                if(getX()+28>>5 == ((mapa.llama.get(i).getX())>> 5) - r && (getY()+25>>5 == mapa.llama.get(i).getY()>>5 || getY()>>5 == mapa.llama.get(i).getY()>>5)) {
                    return true;
                }

                if((getX()+30>>5 == mapa.llama.get(i).getX() >> 5 || getX()>>5 == mapa.llama.get(i).getX() >> 5 ) && getY() >> 5 == (mapa.llama.get(i).getY()>>5) + r) {
                    return true;
                }
                
                if((getX()+30>>5 == mapa.llama.get(i).getX() >> 5 || getX()>>5 == mapa.llama.get(i).getX() >> 5 ) && getY() >> 5 == (mapa.llama.get(i).getY()>>5) - r) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean colision(int dx, int dy) {
        return false;
    }

}
