package bomberman.objetos;

import bomberman.Mapa;
import bomberman.Tile;
import bomberman.objetoGrafico.ObjetoGrafico;

public class Puerta extends ObjetoGrafico{
    private boolean bandera;
    Tile puertaTile = Tile.PUERTA;
    
    public Puerta(boolean puerta,Mapa mapa){
        this.bandera=puerta;
        this.mapa=mapa;
    }
    
    public void addPuerta(int posicion){
        mapa.tilesCatalogo[posicion] = puertaTile;
        bandera=false;
    }
    
    public void setPuerta(boolean puerta){
        this.bandera=puerta;
    }
    
    public boolean getPuerta(){
        return this.bandera;
    }

    public boolean colision(int dx, int dy) {
        return false;
    }
}
