package bomberman.objetos;

import bomberman.Pantalla;
import bomberman.Mapa;
import bomberman.objetoGrafico.ObjetoGrafico;
import java.util.ArrayList;
import java.util.List;
import sprite.Sprite;


public class Llama extends ObjetoGrafico{
    private int counter, rango;
        private List<Integer> rightBreakable;
        private List<Integer> leftBreakable;
        private List<Integer> downBreakable;
        private List<Integer> upBreakable;
	private int r = -1, l = -1, d = -1, u = -1;
        private Bonus bonus;
	
	public Llama(int x, int y, int rango, Sprite sprite, Mapa mapa) {
		setX(x);
		setY(y);
		this.sprite = sprite;
		counter = 0;
		this.mapa = mapa;
		this.rango = rango;
                rightBreakable = new ArrayList<Integer>(); 
                leftBreakable = new ArrayList<Integer>();
                downBreakable = new ArrayList<Integer>();
                upBreakable = new ArrayList<Integer>();
	}
	
	public void actualizar(Pantalla pantalla) {
		counter++;
                
		for(int i = 1; i <= rango; i++) {
			if(r == -1) {
                            if(mapa.getTilesCatalogo(( ((getX()>>5)+i)+(getY()>>5)*25) ).isSolid()) r = i - 1;
                            if(mapa.getTilesCatalogo((((getX()>>5)+i)+(getY()>>5)*25) ).isBreakable()) rightBreakable.add(i);
			}
			if(l == -1) {
				if(mapa.getTilesCatalogo((((getX()>>5)-i)+(getY()>>5)*25) ).isSolid()) l = i - 1;
				if(mapa.getTilesCatalogo((((getX()>>5)-i)+(getY()>>5)*25) ).isBreakable()) leftBreakable.add(i);
			}
			if(d == -1) {
				if(mapa.getTilesCatalogo((getX()>>5)+((getY()>>5)+i)*25).isSolid()) d = i - 1;
				if(mapa.getTilesCatalogo((getX()>>5)+((getY()>>5)+i)*25).isBreakable()) downBreakable.add(i);
			}
			if(u == -1) {
				if(mapa.getTilesCatalogo((getX()>>5)+((getY()>>5)-i)*25).isSolid()) u = i - 1;
				if(mapa.getTilesCatalogo((getX()>>5)+((getY()>>5)-i)*25).isBreakable()) upBreakable.add(i);
			}
                        
                        
			if(i == rango) {
                            //Los lados que no fueron solidos, se igualan a i
                            if(r == -1) r = i;
                            if(l == -1) l = i;
                            if(d == -1) d = i;
                            if(u == -1) u = i;
			}
		}
		
		for(int i = 1; i <= rango; i++) {
			if(r >= i){
                            if(i==rango){
                                sprite = Sprite.EXPLOSION_DERECHA_SPRITE;
                            }
                            else{
                                sprite = Sprite.EXPLOSION_ALARGUE_HORIZONTAL;
                            }
                            pantalla.mostrarLlama((getX()+32)+(32*(i-1)), getY(), this);
                        } 
			if(l >= i){
                            if(i==rango){
                                sprite = Sprite.EXPLOSION_IZQUIERDA_SPRITE;
                            }
                            else{
                                sprite = Sprite.EXPLOSION_ALARGUE_HORIZONTAL;
                            }
                            pantalla.mostrarLlama((getX()-32)-(32*(i-1)), getY(), this);
                        }
			if(d >= i){
                            if(i==rango){
                                sprite = Sprite.EXPLOSION_ABAJO_SPRITE;
                            }
                            else{
                                sprite = Sprite.EXPLOSION_ALARGUE_VERTICAL;
                            }
                            pantalla.mostrarLlama(getX(),(getY()+32)+(32*(i-1)), this);
                        }
			if(u >= i){
                            if(i==rango){
                                sprite = Sprite.EXPLOSION_ARRIBA_SPRITE;
                            }
                            else{
                                sprite = Sprite.EXPLOSION_ALARGUE_VERTICAL;
                            }
                            pantalla.mostrarLlama(getX(), (getY()-32)-(32*(i-1)), this);
                        }
                        
                        sprite = Sprite.EXPLOSION_CENTRO_SPRITE;
                        pantalla.mostrarLlama(getX(), getY(), this);
			
			if(counter == 15) {
                            eliminar();
                            //Los primeros dos elementos que se pasan son para controlar en la clase Bonus si anteriormente ya habia un bonus
                            //En caso de que no haya habido un bonus, el posible bonus se va a colocar en la posicion que tiene el tercer elemento
                            //rightBreakable.get(0)*32 es donde van a aparecer los fantasmas si se explota el bonus
                            while(rightBreakable.size()>0){
                                bonus = new Bonus(getX()+rightBreakable.get(0)*32,getY(),(((getX()>>5)+rightBreakable.get(0))+(getY()>>5)*25),mapa);
                                rightBreakable.remove(0);
                            }
                            while(leftBreakable.size()>0){
                                bonus = new Bonus(getX()-leftBreakable.get(0)*32,getY(),(((getX()>>5)-leftBreakable.get(0))+(getY()>>5)*25),mapa);
                                leftBreakable.remove(0);
                            }
                            while(downBreakable.size()>0){
                                bonus = new Bonus(getX(),getY()+downBreakable.get(0)*32,(getX()>>5)+((getY()>>5)+downBreakable.get(0))*25,mapa);
                                downBreakable.remove(0);
                            }
                            while(upBreakable.size()>0){
                                bonus = new Bonus(getX(),getY()-upBreakable.get(0)*32,(getX()>>5)+((getY()>>5)-upBreakable.get(0))*25,mapa);
                                upBreakable.remove(0);
                            }
			}	
		}
                
                
	}
	
	public int getRango() {
		return rango;
	}
	
	public boolean colisionLlama(int x, int y) {
		for(int i = 1; i <= rango; i++) {
			if((x+30>>5 == getX() >> 5 || x>>5 == getX() >> 5 ) && (y+25>>5 == getY() >> 5 || y>>5 == getY() >> 5)) {
                            return true;
			}
			if(r >= i) {
				if(x+5>>5 == (getX() >> 5) + i && (y+25>>5 == getY() >> 5 || y>>5 == getY() >> 5)) {
                                    return true;
				}
			}
			if(l >= i ) {
				if(x+28>>5 == ((getX())>> 5) - i && (y+25>>5 == getY() >> 5 || y>>5 == getY() >> 5)) {
                                    return true;
				}
			}
			if(d >= i ) {
				if((x+30>>5 == getX() >> 5 || x>>5 == getX() >> 5 ) && y>>5 == (getY() >> 5) + i) {
                                    return true;
				}
			}
			if(u >= i ) {
				if((x+30>>5 == getX() >> 5 || x>>5 == getX() >> 5 ) && y+30>>5 == (getY() >> 5) - i) {
                                    return true;
				}
			}
		}
		
		return false;
	}

    @Override
    public boolean colision(int dx, int dy) {
        return false;
    }
}