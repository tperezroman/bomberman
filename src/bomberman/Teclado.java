package bomberman;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Teclado implements KeyListener{
    private boolean[] teclas = new boolean[120];

    public boolean arriba;
    public boolean abajo;
    public boolean izquierda;
    public boolean derecha;
    public boolean salir;
    public boolean bomba;
    public boolean interruptor;
    
    public void actualizar(){
        arriba = teclas[KeyEvent.VK_W];
        abajo = teclas[KeyEvent.VK_S];
        izquierda = teclas[KeyEvent.VK_A];
        derecha = teclas[KeyEvent.VK_D];
        salir = teclas[KeyEvent.VK_ESCAPE];
        bomba = teclas[KeyEvent.VK_SPACE];
        interruptor = teclas[KeyEvent.VK_F];
    }
    
    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        teclas[e.getKeyCode()] = true;
    }

    public void keyReleased(KeyEvent e) {
        teclas[e.getKeyCode()] = false;
    }
}
