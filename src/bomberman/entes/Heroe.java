package bomberman.entes;

import bomberman.objetoGrafico.ObjetoGraficoMovible;
import bomberman.objetos.Bomba;
import bomberman.Bomberman;
import bomberman.Mapa;
import bomberman.Pantalla;
import bomberman.FXPlayer;
import sprite.Sprite;
import bomberman.Teclado;
import bomberman.Tile;

public class Heroe extends ObjetoGraficoMovible {
    private Teclado teclado;
    private int animacion;
    private Bomba bomba;
    private int bombasDisponibles = 1;
    private int bombasTotales = 1;
    private Tile tile;
    private int velocidad = 2;
    private int rangoBomba = 1;
    private int vidas = 3;
    private boolean planto = false;
    private boolean salto = false;
    private boolean interruptor = false;
    private boolean murio = false;
    private Bomberman bomberman;
    private int contador_nivel = 1;
    private int contador = 0;
    private boolean turno;
    private boolean multiplayer;
    
    public Heroe(Mapa mapa, Teclado teclado, int x, int y, Sprite sprite,Bomberman bomberman, boolean turno,boolean multiplayer) {//x,y pos donde va a aparecer el jugador en el Mapa
        this.mapa = mapa;
        this.teclado = teclado;
        this.sprite = sprite;
        this.turno=turno;
        this.multiplayer=multiplayer;
        setX(x);
        setY(y);
        this.animacion = 0;
        this.bomberman=bomberman;
        
    }

    public void actualizar() {
        int dx = 0;
        int dy = 0;
        if (animacion == 32767)
        {
            animacion = 0;
        }
        animacion++;
    
        if (!murio) {
            if (teclado.arriba) {
                dy = dy - velocidad;
                if (!FXPlayer.PASOVERTICAL.isRunning()) {
                    FXPlayer.PASOVERTICAL.play();
                }
            }
            if (teclado.abajo) {
                dy = dy + velocidad;
                if (!FXPlayer.PASOVERTICAL.isRunning()) {
                    FXPlayer.PASOVERTICAL.play();
                }
            }
            if (teclado.derecha) {
                dx = dx + velocidad;
                if (!FXPlayer.PASOHORIZONTAL.isRunning()) {
                    FXPlayer.PASOHORIZONTAL.play();
                }
            }
            if (teclado.izquierda) {
                dx = dx - velocidad;
                if (!FXPlayer.PASOHORIZONTAL.isRunning()) {
                    FXPlayer.PASOHORIZONTAL.play();
                }
            }
            if (teclado.bomba) {
                FXPlayer.PLANTAR.play();
                if (bombasDisponibles > 0 && !checkBomba()) {
                    murio = false;
                    plantarBomba();
                    planto = true;
                    bombasDisponibles--;
                    //Guardo la direccion donde se pone la bomba, para despues poner el tile solido
                    mapa.posBomba.add((mapa.bomba.get(bombasTotales - bombasDisponibles - 1).getX() >> 5) + (mapa.bomba.get(bombasTotales - bombasDisponibles - 1).getY() >> 5) * 25);
                }
            }

            //Se le pone el tile solido a la bomba, para que no pueda pasarse por encima 
            if (mapa.bomba.size() > 0) {
                //Controlo que el heroe ya no este sobre la bomba
                if (planto && (mapa.bomba.get(bombasTotales - bombasDisponibles - 1).getX() + 26 < getX() || mapa.bomba.get(bombasTotales - bombasDisponibles - 1).getX() - 26 > getX() || mapa.bomba.get(bombasTotales - bombasDisponibles - 1).getY() + 30 < getY() || mapa.bomba.get(bombasTotales - bombasDisponibles - 1).getY() - 30 > getY())) {
                    mapa.tilesCatalogo[(mapa.bomba.get(bombasTotales - bombasDisponibles - 1).getX() >> 5) + (mapa.bomba.get(bombasTotales - bombasDisponibles - 1).getY() >> 5) * 25] = Tile.SUELO_SOLIDO;
                    this.planto = false;
                }
            }

            if (dx != 0 || dy != 0 && !isDelete()) {//si alguno de los dos NO es cero entonces nos movemos
                isMoving = true;
                mover(dx, dy);
            } else {
                isMoving = false;
            }
            
            //COMPROBAR SI ES MULTIPLAYER O SINGLEPLAYER POR LOS SPRITES
            if(multiplayer){
                if (direccion == 'n') {
                    if(bomberman.getTurnoH1()){
                    sprite = Sprite.HEROE_ARRIBA1_SPRITE;
                    if (isMoving) {
                        //mientras mas altos sean los nros mas lenta va a ser la animacion, la dif siempre debe ser la mitad
                        if (animacion % 40 < 20) {//para que la animacion de transicion no sea tan rapida
                            sprite = Sprite.HEROE_ARRIBA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE_ARRIBA3_SPRITE;
                        }
                    }
                    }
                    else{//sino es el turno del heroe2
                        sprite = Sprite.HEROE2_ARRIBA1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE2_ARRIBA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE2_ARRIBA3_SPRITE;
                        }
                    }
                    }
                }
                if (direccion == 's') {
                    if(bomberman.getTurnoH1()){
                    sprite = Sprite.HEROE_ABAJO1_SPRITE;
                        if (isMoving) {
                            if (animacion % 40 < 20) {//para que la animacion de transicion no sea tan rapida
                                sprite = Sprite.HEROE_ABAJO2_SPRITE;
                            } else {
                                sprite = Sprite.HEROE_ABAJO3_SPRITE;
                            }
                        }
                    }
                    else{
                        sprite = Sprite.HEROE2_ABAJO1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE2_ABAJO2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE2_ABAJO3_SPRITE;
                        }
                    }
                    }
                }
                if (direccion == 'e') {
                    if(bomberman.getTurnoH1()){
                    sprite = Sprite.HEROE_DERECHA1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE_DERECHA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE_DERECHA3_SPRITE;
                        }
                    }
                    }else{
                        sprite = Sprite.HEROE2_DERECHA1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE2_DERECHA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE2_DERECHA3_SPRITE;
                        }
                    }
                    }
                }
                if (direccion == 'o') {
                    if(bomberman.getTurnoH1()){
                    sprite = Sprite.HEROE_IZQUIERDA1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE_IZQUIERDA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE_IZQUIERDA3_SPRITE;
                        }
                    }
                    }else{
                        sprite = Sprite.HEROE2_IZQUIERDA1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE2_IZQUIERDA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE2_IZQUIERDA3_SPRITE;
                        }
                    }
                    }
                }
            }
            else if(!multiplayer){
                if (direccion == 'n') {
                    sprite = Sprite.HEROE_ARRIBA1_SPRITE;
                    if (isMoving) { 
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE_ARRIBA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE_ARRIBA3_SPRITE;
                        }
                    }
                }
                if (direccion == 's') {
                    sprite = Sprite.HEROE_ABAJO1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE_ABAJO2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE_ABAJO3_SPRITE;
                        }
                    }
                }
                if (direccion == 'e') {
                    sprite = Sprite.HEROE_DERECHA1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE_DERECHA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE_DERECHA3_SPRITE;
                        }
                    }
                }
                if (direccion == 'o') {
                    sprite = Sprite.HEROE_IZQUIERDA1_SPRITE;
                    if (isMoving) {
                        if (animacion % 40 < 20) {
                            sprite = Sprite.HEROE_IZQUIERDA2_SPRITE;
                        } else {
                            sprite = Sprite.HEROE_IZQUIERDA3_SPRITE;
                        }
                    }
                }
            }
            clear();

            for (int i = 0; i < mapa.llama.size() && mapa.getNivel()!=3; i++) {
                if (mapa.llama.get(i).colisionLlama(getX(), getY()) && !murio) {
                    FXPlayer.MUERTE.play();
                    matarHeroe();
                    break;
                }
            }

            if (creepColision() && !murio) {
                matarHeroe();
                FXPlayer.MUERTE.play();
            }
            
            if(vidas==0){
                this.eliminar();
                FXPlayer.GAMEOVER.play();
            }
        }
        else{
            //CONTROLA LOS SPRITES DE MUERTE
            if(multiplayer){
                contador++;
                if(bomberman.getTurnoH1()){
                    if (contador % 50 < 10) {
                        sprite = Sprite.HEROE_MUERTE1_SPRITE;
                    } else if (contador % 50 < 20) {
                        sprite = Sprite.HEROE_MUERTE2_SPRITE;
                    } else if (contador % 50 < 30) {
                        sprite = Sprite.HEROE_MUERTE3_SPRITE;
                    } else if (contador % 50 < 40) {
                        sprite = Sprite.HEROE_MUERTE4_SPRITE;
                        murio = false;
                        contador = 0;
                        restablecerMapa();
                    }
                }
                else if(bomberman.getTurnoH2()){
                    if (contador % 50 < 10) {
                        sprite = Sprite.HEROE2_MUERTE1_SPRITE;
                    } else if (contador % 50 < 20) {
                        sprite = Sprite.HEROE2_MUERTE2_SPRITE;
                    } else if (contador % 50 < 30) {
                        sprite = Sprite.HEROE2_MUERTE3_SPRITE;
                    } else if (contador % 50 < 40) {
                        sprite = Sprite.HEROE2_MUERTE4_SPRITE;
                        murio = false;
                        contador = 0;
                        restablecerMapa();
                    }
                }
            }
            else if(!multiplayer){
                contador++;
                if (contador % 50 < 10) {
                    sprite = Sprite.HEROE_MUERTE1_SPRITE;
                } else if (contador % 50 < 20) {
                    sprite = Sprite.HEROE_MUERTE2_SPRITE;
                } else if (contador % 50 < 30) {
                    sprite = Sprite.HEROE_MUERTE3_SPRITE;
                } else if (contador % 50 < 40) {
                    sprite = Sprite.HEROE_MUERTE4_SPRITE;
                    murio = false;
                    contador = 0;
                    restablecerMapa();
                }
            }
        }      
    }

    public void matarHeroe() {
        if(multiplayer){
            if(bomberman.getTurnoH1() && bomberman.getVidasH1()!=0){
                vidas--;
                if(bomberman.getVidasH2()!=0){
                    bomberman.setLabelVidasH2();
                    turno=false;
                    bomberman.setTurnoH2(true);
                }
                else{
                    bomberman.setLabelVidasH1();
                }
            }
            else if(bomberman.getTurnoH2() && bomberman.getVidasH2()!=0){
                vidas--;
                if(bomberman.getVidasH1()!=0){
                    bomberman.setLabelVidasH1();
                    turno=false;
                    bomberman.setTurnoH1(true);
                }
                else{
                    bomberman.setLabelVidasH2();
                }
            }
        }
        else{
            vidas--;
            bomberman.setLabelVidasH1();
        }
        
        murio = true;
        salto = false;
        interruptor = false;
        planto=false;
        velocidad = 2;
        bombasTotales = 1;
        bombasDisponibles = 1;
        rangoBomba = 1;
    }

    public void restablecerMapa() {
        for (int p = 0; p < mapa.creep_rosa.size(); p++) {
            mapa.creep_rosa.get(p).eliminar();
        }
        for (int p = 0; p < mapa.creep_celeste.size(); p++) {
            mapa.creep_celeste.get(p).eliminar();
        }
        for (int p = 0; p < mapa.bomba.size(); p++) {
            mapa.bomba.get(p).eliminar();
        }
        for (int p = 0; p < mapa.llama.size(); p++) {
            mapa.llama.get(p).eliminar();
        }
        
        if (mapa.getNivel() == 1) {
            mapa.cargarMapa("/imagenes/mapas/Nivel_" + bomberman.getNivel() + ".png");
            mapa.generarMapa();
            mapa.addFantasmaRosa(6);
        } 
        else if(mapa.getNivel() == 2) {
            mapa.cargarMapa("/imagenes/mapas/Nivel_" + bomberman.getNivel() + ".png");
            mapa.generarMapa();
            mapa.addFantasmaRosa(3);
            mapa.addFantasmaCeleste(3);
        }
        else{
            mapa.cargarMapa("/imagenes/mapas/extra.png");
            mapa.generarMapa();
        }
        setX(32);
        setY(32);
        bomberman.setTemporizador(200);
        mapa.setPuerta(true);
    }

    public void siguienteNivel() {
        if(mapa.creep_rosa.isEmpty() && mapa.creep_celeste.isEmpty() && vidas>0){
            for (int p = 0; p < mapa.bomba.size(); p++) {
                mapa.bomba.get(p).eliminar();
            }
            for (int p = 0; p < mapa.llama.size(); p++) {
                mapa.llama.get(p).eliminar();
            }
            FXPlayer.PUERTA.play();
            
            bomberman.setPuntos(200);
            mapa.setNivel(mapa.getNivel()+1);
            bomberman.setNivel(bomberman.getNivel() % 4 + 1);
            
            if (mapa.getNivel() == 3) {
                mapa.cargarMapa("/imagenes/mapas/extra.png");
                mapa.generarMapa();
                setX(32);
                setY(32);
                planto=false;
                mapa.tilesCatalogo[37] = Tile.PUERTA;
            } 
            else {
                mapa.cargarMapa("/imagenes/mapas/Nivel_" + bomberman.getNivel() + ".png");
                mapa.generarMapa();
                setX(32);
                setY(32);
                planto=false;
                mapa.addFantasmaRosa(3);
                mapa.addFantasmaCeleste(3);
                bomberman.setTemporizador(200);
                mapa.setPuerta(true);
            }

            
	}
        
    }

    //Controla que no se ponga mas de una bomba en la misma posicion
    public boolean checkBomba() {
        for (int i = 0; i < mapa.bomba.size(); i++) {
            if ((getX()) >= mapa.bomba.get(i).getX() && (getX()) < (mapa.bomba.get(i).getX() + 32) && (getY() + 10) >= mapa.bomba.get(i).getY() && (getY() + 10) < (mapa.bomba.get(i).getY() + 32)) {
                return true;
            }
            if ((getX() + 32) >= mapa.bomba.get(i).getX() && (getX() + 32) < (mapa.bomba.get(i).getX() + 32) && (getY() + 10) >= mapa.bomba.get(i).getY() && (getY() + 10) < (mapa.bomba.get(i).getY() + 32)) {
                return true;
            }
            if ((getX()) >= mapa.bomba.get(i).getX() && (getX()) < (mapa.bomba.get(i).getX() + 32) && (getY()  + 20) >= mapa.bomba.get(i).getY() && (getY() + 10 + 32) < (mapa.bomba.get(i).getY() + 32)) {
                return true;
            }
            if ((getX() + 32) >= mapa.bomba.get(i).getX() && (getX() + 32) < (mapa.bomba.get(i).getX() + 32) && (getY() + 10 + 32) >= mapa.bomba.get(i).getY() && (getY() + 10 + 32) < (mapa.bomba.get(i).getY() + 32)) {
                return true;
            }
        }
        return false;
    }

    private boolean creepColision() {
        for (int i = 0; i < mapa.creep_rosa.size(); i++) {
            if ((getX()) + 28 >> 5 == mapa.creep_rosa.get(i).getX()+5 >> 5 && ((getY() + 28 >> 5 == mapa.creep_rosa.get(i).getY() >> 5) || getY() >> 5 == mapa.creep_rosa.get(i).getY() >> 5)) {
                return true;
            }
            if (getX()>> 5 == mapa.creep_rosa.get(i).getX()+5 >> 5 && ((getY() + 28 >> 5 == mapa.creep_rosa.get(i).getY() >> 5) || getY() >> 5 == mapa.creep_rosa.get(i).getY() >> 5)) {
                return true;
            }
            
        }
        for (int i = 0; i < mapa.creep_celeste.size(); i++) {
            if ((getX() + 28) >> 5 == mapa.creep_celeste.get(i).getX()+5 >> 5 && ((getY() + 28 >> 5 == mapa.creep_celeste.get(i).getY() >> 5) || getY() >> 5 == mapa.creep_celeste.get(i).getY() >> 5)) {
                return true;
            }
            if (getX() >> 5 == mapa.creep_celeste.get(i).getX()+5 >> 5 && ((getY() + 28 >> 5 == mapa.creep_celeste.get(i).getY() >> 5) || getY() >> 5 == mapa.creep_celeste.get(i).getY() >> 5)) {
                return true;
            }
        }
        return false;
    }

    private void plantarBomba() {
        bomba = new Bomba(((getX() + 20) >> 5) << 5, ((getY() + 20) >> 5) << 5, Sprite.BOMBA1_SPRITE, mapa, rangoBomba,this,teclado);
        mapa.addBomba(bomba);
    }

    private void clear() {
        for (int i = 0; i < mapa.bomba.size(); i++) {
            if (mapa.bomba.get(i).isDelete() ) {
                mapa.bomba.remove(i);
                mapa.tilesCatalogo[mapa.posBomba.get(i)] = Tile.SUELO;
                mapa.posBomba.remove(i);
                if(bombasDisponibles<bombasTotales){
                    bombasDisponibles++;
                }
                
            }
        }

        for (int i = 0; i < mapa.llama.size(); i++) {
            if (mapa.llama.get(i).isDelete()) {
                mapa.llama.remove(i);
            }
        }
    }

    public void mostrar(Pantalla pantalla) {
        pantalla.mostrarHeroe(getX(), getY(), this);
    }

    public void addRangoBomba() {
        this.rangoBomba++;
    }

    public void addBombaDisponible() {
        this.bombasDisponibles++;
        this.bombasTotales++;
    }

    public void addVidas() {
        vidas ++;
        
        if(bomberman.getTurnoH1()){
            bomberman.setLabelVidasH1();
        }
        else if(bomberman.getTurnoH2()){
            bomberman.setLabelVidasH2();
        }
    }
    
    public void setVidas(int vidas){
        this.vidas=vidas;
    }
    
    public int getVidas() {
        return this.vidas;
    }
    
    public boolean getInterruptor() {
        return interruptor;
    }
    
    public boolean getSalto() {
        return salto;
    }
    
    public boolean getTurno() {
        return turno;
    }
    
    public void setTurno(boolean turno){
        this.turno=turno;
    }
    
    public void setMuerte(boolean muerte){
        this.murio=muerte;
    }

    public boolean colision(int dx, int dy) {
        boolean colision = false;
        
        int posX = getX() + dx;
        int posY = getY() + dy;

        int margenIzquierdo = -25;
        int margenDerecho = 28;
        
        int margenSuperior = -30;
        int margenInferior = 31;

        int bordeIzquierdo = (posX + margenDerecho) / sprite.obtenerAncho();
        int bordeDerecho = (posX + margenDerecho + margenIzquierdo) / sprite.obtenerAncho();
        
        int bordeSuperior = (posY + margenInferior) / sprite.obtenerAncho();
        int bordeInferior = (posY + margenInferior + margenSuperior) / sprite.obtenerAncho();    
        
        if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()).isSolid()){
            colision = true;
        }
        if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()).isSolid()){
            colision = true;
        }
        
        if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()).isSolid()){
            colision = true;
        }
        if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()).isSolid()){
            colision = true;
        }
        
            //////Colisiones con los bonus/////
            if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()) == Tile.BONUS_BOMBA_EXTRA){
                mapa.tilesCatalogo[bordeIzquierdo + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                addBombaDisponible();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()) == Tile.BONUS_BOMBA_EXTRA){ 
                mapa.tilesCatalogo[bordeIzquierdo + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                addBombaDisponible();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }

            else if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()) == Tile.BONUS_BOMBA_EXTRA){
                mapa.tilesCatalogo[bordeDerecho + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                addBombaDisponible();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()) == Tile.BONUS_BOMBA_EXTRA){
                mapa.tilesCatalogo[bordeDerecho + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                addBombaDisponible();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            ///////////////////////////////////////////////////////////////////////////////////////
            if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()) == Tile.BONUS_DETONADOR){
                mapa.tilesCatalogo[bordeIzquierdo + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                interruptor=true;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()) == Tile.BONUS_DETONADOR){ 
                mapa.tilesCatalogo[bordeIzquierdo + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                interruptor=true;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }

            else if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()) == Tile.BONUS_DETONADOR){
                mapa.tilesCatalogo[bordeDerecho + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                interruptor=true;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()) == Tile.BONUS_DETONADOR){
                mapa.tilesCatalogo[bordeDerecho + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                interruptor=true;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            ///////////////////////////////////////////////////////////////////////////////////////
            if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()) == Tile.BONUS_RANGO){
                mapa.tilesCatalogo[bordeIzquierdo + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                addRangoBomba();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
                
            }
            else if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()) == Tile.BONUS_RANGO){ 
                mapa.tilesCatalogo[bordeIzquierdo + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                addRangoBomba();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }

            else if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()) == Tile.BONUS_RANGO){
                mapa.tilesCatalogo[bordeDerecho + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                addRangoBomba();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()) == Tile.BONUS_RANGO){
                mapa.tilesCatalogo[bordeDerecho + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                addRangoBomba();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            ///////////////////////////////////////////////////////////////////////////////////////
            if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()) == Tile.BONUS_SALTO){
                mapa.tilesCatalogo[bordeIzquierdo + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                salto=true;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()) == Tile.BONUS_SALTO){ 
                mapa.tilesCatalogo[bordeIzquierdo + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                salto=true;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }

            else if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()) == Tile.BONUS_SALTO){
                mapa.tilesCatalogo[bordeDerecho + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                salto=true;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()) == Tile.BONUS_SALTO){
                mapa.tilesCatalogo[bordeDerecho + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                salto=true;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            ///////////////////////////////////////////////////////////////////////////////////////
            if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()) == Tile.BONUS_VELOCIDAD){
                mapa.tilesCatalogo[bordeIzquierdo + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                velocidad=3;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()) == Tile.BONUS_VELOCIDAD){ 
                mapa.tilesCatalogo[bordeIzquierdo + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                velocidad=3;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }

            else if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()) == Tile.BONUS_VELOCIDAD){
                mapa.tilesCatalogo[bordeDerecho + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                velocidad=3;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()) == Tile.BONUS_VELOCIDAD){
                mapa.tilesCatalogo[bordeDerecho + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                velocidad=3;
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            ///////////////////////////////////////////////////////////////////////////////////////
            if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()) == Tile.BONUS_VIDA){
                mapa.tilesCatalogo[bordeIzquierdo + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                addVidas();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()) == Tile.BONUS_VIDA){ 
                mapa.tilesCatalogo[bordeIzquierdo + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                addVidas();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }

            else if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()) == Tile.BONUS_VIDA){
                mapa.tilesCatalogo[bordeDerecho + bordeSuperior * mapa.getAncho()] = Tile.SUELO;
                addVidas();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            else if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()) == Tile.BONUS_VIDA){
                mapa.tilesCatalogo[bordeDerecho + bordeInferior * mapa.getAncho()] = Tile.SUELO;
                addVidas();
                bomberman.setPuntos(50);
                FXPlayer.BONUS.play();
            }
            
            //COLISION CON PUERTA
            if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()) == Tile.PUERTA){
                siguienteNivel();
            }
            else if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()) == Tile.PUERTA){ 
                siguienteNivel();
            }

            else if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()) == Tile.PUERTA){
                siguienteNivel();
            }
            else if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()) == Tile.PUERTA){
                siguienteNivel();
            }
            
        return colision;
    }

    public void mover(int dx, int dy){//desplazamiento de un diferencial de x (dx) o dy
        if(dx > 0){
            direccion = 'e';
        }
        if(dx < 0){
            direccion = 'o';
        }
        if(dy > 0){
            direccion = 's';
        }
        if(dy < 0){
            direccion = 'n';
        }
        if(!isDelete()){
            if(!salto){
                Tile.SUELO_SOLIDO.setSolid(true);
            }else{
                Tile.SUELO_SOLIDO.setSolid(false);
            }
            for(int i=0; i<Math.abs(dx)-1; i++){
                if(dx<0){
                    if(!colision(-2+i,0)){
                        moverX(-2+i);
                    }
                }
                else if(dx>0){
                    if(!colision(2-i,0)){
                        moverX(2-i);
                    }
                }
            }
            for(int i=0; i<Math.abs(dy)-1; i++){
                if(dy<0){
                    if(!colision(0,-2+i)){
                        moverY(-2+i);
                    }
                }
                else if(dy>0){
                    if(!colision(0,2-i)){
                        moverY(2-i);
                    }
                }
            }
        }
    }
}
