package bomberman.entes;

import bomberman.Pantalla;
import bomberman.Mapa;
import sprite.Sprite;

public class Fantasma_Celeste extends Fantasma{
    public Fantasma_Celeste(Mapa mapa, int x, int y, Sprite sprite) {
        super(mapa, x, y, sprite);
    }
    
    public void actualizar(Pantalla pantalla) {	
		xa = 0;
		ya = 0;
		
		if(animacion < 7500) animacion++;
		else animacion = 0;
		
		
                if(!eliminado){
                    actualizarDir();
                    generarDirRandom();
                    darPuntos=true;
                    if(animacion % 60 > 50) {
                        sprite = Sprite.FANTASMA2_MOVIMIENTO1_SPRITE;
                    }else if(animacion % 60 > 40) {
                            sprite = Sprite.FANTASMA2_MOVIMIENTO2_SPRITE;
                    }else if(animacion % 60 > 30) {
                            sprite = Sprite.FANTASMA2_MOVIMIENTO3_SPRITE;
                    }else if(animacion % 60 > 20) {
                            sprite = Sprite.FANTASMA2_MOVIMIENTO4_SPRITE;
                    }else if(animacion % 60 > 10) {
                            sprite = Sprite.FANTASMA2_MOVIMIENTO5_SPRITE;
                    }else {
                            sprite = Sprite.FANTASMA2_MOVIMIENTO6_SPRITE;
                    }
                }
                else{
                    animacion_muerte ++;
                    if(animacion_muerte % 70 < 10) {
                        sprite = Sprite.FANTASMA2_MUERTE_SPRITE;
                    }else if(animacion_muerte % 70 < 20) {
                            sprite = Sprite.FANTASMA2_MUERTE_SPRITE;
                    }else if(animacion_muerte % 70 < 30) {
                            sprite = Sprite.FANTASMA2_MUERTE_SPRITE;
                    }else if(animacion_muerte % 70 < 40) {
                            sprite = Sprite.FANTASMA2_MUERTE_SPRITE;
                    }else if(animacion_muerte % 70 < 50) {
                            sprite = Sprite.FANTASMA2_MUERTE_SPRITE;
                    }else {
                            sprite = Sprite.FANTASMA2_MUERTE_SPRITE;
                            eliminar();
                    }
                }
                pantalla.mostrarFantasmas(getX(), getY(), this);
                
                for(int i = 0; i< mapa.llama.size(); i++) {
                    if(mapa.llama.get(i).colisionLlama(getX(), getY()) && darPuntos) {
                        mapa.setPuntos(150);
                        eliminado=true;
                        darPuntos=false;
                        break;
                    }
		}
		
	}
}