package bomberman.entes;


import bomberman.Pantalla;
import bomberman.Mapa;
import sprite.Sprite;

public class Fantasma_Rosa extends Fantasma{
    
    public Fantasma_Rosa(Mapa mapa, int x, int y, Sprite sprite) {
        super(mapa, x, y, sprite);
    }
    
    public void actualizar(Pantalla pantalla) {	
		xa = 0;
		ya = 0;
		
		if(animacion < 7500) animacion++;
		else animacion = 0;
		
		
                if(!eliminado){
                    actualizarDir();
                    generarDirRandom();
                    darPuntos=true;
                    if(animacion % 60 > 50) {
                        sprite = Sprite.FANTASMA1_MOVIMIENTO1_SPRITE;
                    }else if(animacion % 60 > 40) {
                            sprite = Sprite.FANTASMA1_MOVIMIENTO2_SPRITE;
                    }else if(animacion % 60 > 30) {
                            sprite = Sprite.FANTASMA1_MOVIMIENTO3_SPRITE;
                    }else if(animacion % 60 > 20) {
                            sprite = Sprite.FANTASMA1_MOVIMIENTO4_SPRITE;
                    }else if(animacion % 60 > 10) {
                            sprite = Sprite.FANTASMA1_MOVIMIENTO5_SPRITE;
                    }else {
                            sprite = Sprite.FANTASMA1_MOVIMIENTO6_SPRITE;
                    }
                }
                else{
                    animacion_muerte ++;
                    
                    if(animacion_muerte % 70 < 10) {
                        sprite = Sprite.FANTASMA1_MUERTE1_SPRITE;
                    }else if(animacion_muerte % 70 < 20) {
                            sprite = Sprite.FANTASMA1_MUERTE2_SPRITE;
                    }else if(animacion_muerte % 70 < 30) {
                            sprite = Sprite.FANTASMA1_MUERTE3_SPRITE;
                    }else if(animacion_muerte % 70 < 40) {
                            sprite = Sprite.FANTASMA1_MUERTE4_SPRITE;
                    }else if(animacion_muerte % 70 < 50) {
                            sprite = Sprite.FANTASMA1_MUERTE5_SPRITE;
                    }else if(animacion_muerte % 70 < 60){
                            sprite = Sprite.FANTASMA1_MUERTE6_SPRITE;
                            eliminar();
                    }
                }
                
                pantalla.mostrarFantasmas(getX(), getY(), this);
                
                for(int i = 0; i< mapa.llama.size(); i++) {
                    if(mapa.llama.get(i).colisionLlama(getX(), getY()) && darPuntos) {
                            mapa.setPuntos(100);
                            eliminado=true;
                            darPuntos=false;
                            break;
                    }
                }
    }
}

