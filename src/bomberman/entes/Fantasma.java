package bomberman.entes;

import bomberman.objetoGrafico.ObjetoGraficoMovible;
import bomberman.Mapa;
import bomberman.Tile;
import java.util.Random;
import sprite.Sprite;


public class Fantasma extends ObjetoGraficoMovible{
    protected int animacion, xa, ya, dir, animacion_muerte=0;;
    private Random random = new Random();
    protected boolean eliminado=false;
    protected boolean darPuntos=false;
    
    public Fantasma(Mapa mapa, int x, int y, Sprite sprite) {
        this.mapa = mapa;
        this.sprite = sprite;
        setX(x);
        setY(y);
        this.animacion = 0;
    }
    
    
    public void generarDirRandom() {
		if(colision(xa, ya)) dir = random.nextInt(4);
		else mover(xa, ya);
	}
	
    public void actualizarDir() {
            if(dir == 0) ya -= 2;
            else if(dir == 1) xa += 2;
            else if(dir == 2) ya += 2;
            else xa -= 2;
    }

    public boolean colision(int dx, int dy) {
        boolean colision = false;
        
        int posX = getX() + dx;
        int posY = getY() + dy;

        int margenIzquierdo = -25;
        int margenDerecho = 28;
        
        int margenSuperior = -30;
        int margenInferior = 31;

        int bordeIzquierdo = (posX + margenDerecho) / sprite.obtenerAncho();
        int bordeDerecho = (posX + margenDerecho + margenIzquierdo) / sprite.obtenerAncho();
        
        int bordeSuperior = (posY + margenInferior) / sprite.obtenerAncho();
        int bordeInferior = (posY + margenInferior + margenSuperior) / sprite.obtenerAncho();    
        
        if(mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho()).isSolid() || mapa.getTilesCatalogo(bordeIzquierdo + bordeSuperior * mapa.getAncho())==Tile.SUELO_SOLIDO){
            colision = true;
        }
        if(mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho()).isSolid() || mapa.getTilesCatalogo(bordeIzquierdo + bordeInferior * mapa.getAncho())==Tile.SUELO_SOLIDO){ 
            colision = true;
        }
        
        if(mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho()).isSolid() || mapa.getTilesCatalogo(bordeDerecho + bordeSuperior * mapa.getAncho())==Tile.SUELO_SOLIDO){
            colision = true;
        }
        if(mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho()).isSolid() || mapa.getTilesCatalogo(bordeDerecho + bordeInferior * mapa.getAncho())==Tile.SUELO_SOLIDO){
            colision = true;
        }
        
        return colision;
    }

    @Override
    public void mover(int dx, int dy) {
    if(dx > 0){
            direccion = 'e';
        }
        if(dx < 0){
            direccion = 'o';
        }
        if(dy > 0){
            direccion = 's';
        }
        if(dy < 0){
            direccion = 'n';
        }
        if(!isDelete()){//comprobar si el fantasma esta vivo o muerto
            if(!colision(dx,0)){
                moverX(dx);
            }
            if(!colision(0,dy)){
                moverY(dy);
            }
        }
    }
}