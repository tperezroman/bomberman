package bomberman;

import java.awt.Font;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.FileReader;
import javax.swing.JLabel;
import javax.swing.JPanel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.awt.Color;
import java.awt.Graphics;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.swing.ImageIcon;

public class Ranking extends JPanel{
    protected ItemsRanking records[] = new ItemsRanking[10];
    protected JLabel title;
    private Image background_img;
    JsonArray jsonArray = new JsonArray();
    JsonParser jParser = new JsonParser();
    JsonObject jsonObject = new JsonObject();

    public Ranking() {
        this.setLayout(null);
        obtenerRanking();
        ordenarRanking();
        background_img = new ImageIcon("src/imagenes/plataforma/top.jpg").getImage();
    }
    
    public void paintComponent(Graphics g) {
        int paso = 100;
        if(records.length == 0){
            g.drawImage(background_img, 0, 0, getWidth(), getHeight(), null);
            g.setFont(new Font("Helvetica", Font.BOLD, 25));
            g.setColor(Color.white);
            g.drawString("You haven't played yet \n Play to see the ranking", 300, 100);
        }
        else{
            g.drawImage(background_img, 0, 0, getWidth(), getHeight(), null);
            g.setFont(new Font("Helvetica", Font.BOLD, 25));
            g.setColor(Color.white);
            g.drawString("TOP 10:", 300, 40);
            g.drawString("Name", 100, 100);
            g.drawString("Score", 300, 100);
            g.drawString("Date", 500, 100);
            for (int i = 0; i < records.length; i++) {
                paso += 40;
                g.drawString(records[i].getName(), 100, paso);
                g.drawString(records[i].getScore().toString(), 300, paso);
                g.drawString(records[i].getDate(), 500, paso);
            }
        }
    }
    
    // lee el json
    private void obtenerRanking() {
        Gson gson = new Gson();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("src/bomberman/ranking.json"));
            Player resultado = gson.fromJson(br, Player.class);
            if (resultado != null) {
                int i = 0;
                for (ItemsRanking res : resultado.getRecord()) {
                    this.records[i] = res;
                    i++;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void cargarRanking(String nombre, Bomberman bomber) {
        Gson gson = new Gson();
        obtenerRanking();
        insertarRanking(nombre, bomber);
        for (int i = 0; i < records.length; i++) {
            jsonArray.add(jParser.parse(gson.toJson(this.records[i])));
        }
        try {
            jsonObject.add("puntajes", jsonArray);
            FileWriter fw = new FileWriter("src/bomberman/ranking.json");
            fw.write(jsonObject.toString());
            fw.close();

        } catch (IOException e) {
        }
    }
    
    // este metodo inserta al nuevo record en el ranking
    // solamente si es mayor a algun record que encuentre, si es igual lo
    // sobreescribe
    private void insertarRanking(String nombre, Bomberman bomber) {
        ItemsRanking nuevoRecord = new ItemsRanking();
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        nuevoRecord.addRecord(nombre, bomber.getPuntos(), date.format(formatter).toString());
        for (int i = 0; i < records.length; i++) {
            if (nuevoRecord.getScore() >= records[i].getScore()) {
                ItemsRanking aux;
                ItemsRanking aux2 = new ItemsRanking();
                aux = records[i];
                records[i] = nuevoRecord;
                for (int j = i + 1; j < records.length; j++) {
                    aux2 = records[j];
                    records[j] = aux;
                    aux = aux2;
                }
                break;
            }
        }
    }
    
    private void ordenarRanking() {
        ItemsRanking temp = null;
        for (int i = 0; i < this.records.length; i++) {
            for (int j = 0; j < this.records.length-1; j++) {
                if (this.records[j].getScore() < this.records[j + 1].getScore()) {
                    temp = this.records[j];
                    this.records[j] = this.records[j + 1];
                    this.records[j + 1] = temp;
                }
            }
        }
    }
}
