package bomberman;

class ItemsRanking {
    protected int score;
    protected String name;
    protected String date;

    public ItemsRanking() {
        name = "";
        score = 0;
        date = "";
    }

    public Integer getScore() {
        return this.score;
    }

    public String getDate() {
        return this.date;
    }
    
    public String getName() {
        return this.name;
    }

    public void addRecord(String nombre, int score, String date) {
        this.name = nombre;
        this.score = score;
        this.date = date;
    }
}