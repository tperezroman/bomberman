package bomberman;

public class Player {
    private ItemsRanking puntajes[];
    private String nombre;
    
    public Player(String nombre){
        this.nombre=nombre;
    }

    public ItemsRanking[] getRecord(){
        return puntajes;
    }
    
    public String getNombre(){
        return this.nombre;
    }
}
