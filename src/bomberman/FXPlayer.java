package bomberman;

import java.io.*;
import java.net.URL;
import javax.sound.sampled.*;


public enum FXPlayer {
   MENU("menu_principal.wav"),
   LEVEL("Bomberman.wav"),
   START("start.wav"),
   MUERTE("muerte.wav"),
   GAMEOVER("gameover.wav"),
   GANO("fin_juego.wav"),
   PASOHORIZONTAL("pasohorizontal.wav"),
   PASOVERTICAL("pasovertical.wav"),
   SILENCIO("silencio.wav"),
   EXPLOSION("explosion.wav"),
   PLANTAR("plantarbomba.wav"),
   PUERTA("puerta.wav"),
   BONUS("bonus.wav"),
   ;


   public static enum Volume {
      MUTE, LOW, MEDIUM, HIGH
   }

   public static Volume volume = Volume.LOW;


   private Clip clip;


   FXPlayer(String wav) {
      try {
         URL url = new File("src/sonidos/"+wav).toURI().toURL();
         AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url);
         clip = AudioSystem.getClip();

         clip.open(audioInputStream);
      } catch (UnsupportedAudioFileException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      } catch (LineUnavailableException e) {
         e.printStackTrace();
      }
   }


   public void play() {
      if (volume != Volume.MUTE) {
         if (!clip.isRunning()){
         	  clip.setFramePosition(0);
         		clip.start();
         }
      }
   }
   public void stop() {
	   if (clip.isRunning()){
      		clip.stop();
   }
      }
   
   public boolean isRunning() {
	return clip.isRunning();
   }


   static void init() {
      values();
   }
}