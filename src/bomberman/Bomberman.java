package bomberman;

import bomberman.entes.Heroe;
import sprite.Sprite;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;
import javax.swing.*;
import sistemajuegos.MenuInicio;


public class Bomberman extends Canvas implements Runnable{
    
    private Thread thread;
    private volatile boolean corriendo=false;//No podra ser utilizada por mas de un thread
    private JFrame ventana;
    private JPanel panel;
    private MenuInicio menu;
    
    private Teclado teclado;
    private final int width=800;
    private final int height=600;
    private Pantalla pantalla;
    
    private Mapa mapa;
    private BufferedImage imagen = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
    private int[] pixeles = ((DataBufferInt) imagen.getRaster().getDataBuffer()).getData();
    
    private int temporizador=200;
    private int puntos=0;
    //Labels
    private JLabel tiempo;
    private JLabel vida;
    private static JLabel labelPuntos;
    
    private Heroe heroe1;
    private Heroe heroe2;
    private Random random;
    private int nivel;
    private int creepsIniciales=6;
    
    //Es temporal, para controlar los fps
    private String CONTADOR_APS = "";
    private String CONTADOR_FPS = "";
    private int aps=0;
    private int fps=0;
    
    private boolean multiplayer;
    
    public Bomberman(boolean multiplayer){
        FXPlayer.START.play();
        pantalla = new Pantalla(width,height);
        teclado = new Teclado();
        addKeyListener(teclado);
        random = new Random();
        nivel = random.nextInt(3)+1;
        mapa = new Mapa("/imagenes/mapas/Nivel_" + nivel + ".png",this);
        mapa.addFantasmaRosa(creepsIniciales);
        this.multiplayer=multiplayer;
        heroe1 = new Heroe(mapa,teclado,32,32,Sprite.HEROE_ABAJO1_SPRITE,this,true,multiplayer);
        if(multiplayer){
            heroe2 = new Heroe(mapa,teclado,32,32,Sprite.HEROE2_ABAJO1_SPRITE,this,false,multiplayer);
        }
        ventana = new JFrame("Bomberman");
        ventana.setLayout(null);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.setResizable(false);
        
        panel = new JPanel(null);
        panel.setBackground(Color.gray);
        panel.setPreferredSize(new Dimension(800,100));
        vida = new JLabel("P1 "+heroe1.getVidas());
        vida.setForeground(Color.white);
        vida.setFont(new Font("Helvetica", Font.BOLD, 25));
        tiempo = new JLabel("TIME "+temporizador);
        tiempo.setForeground(Color.white);
        tiempo.setFont(new Font("Helvetica", Font.BOLD, 25));
        labelPuntos = new JLabel(""+puntos);
        labelPuntos.setForeground(Color.white);
        labelPuntos.setFont(new Font("Helvetica", Font.BOLD, 25));
        
        panel.add(tiempo);
        panel.add(labelPuntos);
        panel.add(vida);
        
        tiempo.setLocation(0, 60);
        tiempo.setSize(140,30);
        labelPuntos.setLocation(600, 60);
        labelPuntos.setSize(80,30);
        vida.setLocation(700,60);
        vida.setSize(100,30);
        
        //Se carga a la ventana principal el Jpanel y el Canvas
        ventana.setPreferredSize(new Dimension(800,600));
        ventana.add(panel);
        ventana.add(this);
        panel.setSize(800,100);
        panel.setLocation(0,0);
        this.setSize(width,height);
        this.setLocation(0,32);
        
        ventana.setUndecorated(true);
        ventana.pack();
        ventana.setLocationRelativeTo(null);
        ventana.setVisible(true);
        
        
    }
    
    public void mostrar() {
        fps ++;
        BufferStrategy bs = getBufferStrategy();
        if(bs == null) {
                createBufferStrategy(3);
                return;
        }
        
        mapa.mostrar(0,-66, pantalla);
        if(!multiplayer){
            heroe1.mostrar(pantalla);
        }
        else{
            if(heroe1.getTurno()){
                heroe1.mostrar(pantalla);
            }
            if(heroe2.getTurno()){
                heroe2.mostrar(pantalla);
            }
        }
        
        System.arraycopy(pantalla.pixels,0,pixeles,0,pixeles.length);
        Graphics g = bs.getDrawGraphics();
        g.drawImage(imagen, 0, 0, 800,624,null);
        g.setColor(Color.red);
        
        //Temporal
        g.drawString(CONTADOR_FPS, 10, 100);

        g.dispose();
        
        bs.show();
    }
    
    public void actualizar(){//se ejecuta 60 veces por segundo
        aps ++;
        
        teclado.actualizar();
        if(!multiplayer){
            heroe1.actualizar();
        }
        else{
            if(heroe1.getTurno()){
                heroe1.actualizar();
            }
            else if(heroe2.getTurno()){
                heroe2.actualizar();
            }
        }
        
        mapa.actualizar();
        
        if(mapa.getNivel()==4){
            Player jugador = new Player(JOptionPane.showInputDialog(null, "Ingrese su nombre"));
            Ranking cr = new Ranking();
            cr.cargarRanking(jugador.getNombre(), this);
            menu = new MenuInicio();
            ventana.dispose();
            FXPlayer.LEVEL.stop();
            stop();
        }
        if(!multiplayer){
            if(heroe1.isDelete()){
                menu = new MenuInicio();
                ventana.dispose();
                FXPlayer.LEVEL.stop();
                stop();
            }
        }
        else{
            if(heroe1.isDelete() && heroe2.isDelete()){
                menu = new MenuInicio();
                ventana.dispose();
                FXPlayer.LEVEL.stop();
                stop();
            }
        }
        
        if(teclado.salir){
            menu = new MenuInicio();
            ventana.dispose();
            FXPlayer.START.stop();
            FXPlayer.LEVEL.stop();
            stop();
        }
        
    }
    
    public void setTemporizador(int temp){
        this.temporizador=temp;
    }
    
    public int getTemporizador(){
        return this.temporizador;
    }
        
    public void setPuntos(int p){
        this.puntos=this.puntos+p;
        labelPuntos.setText(""+puntos);
    }
    
    public int getPuntos(){
        return puntos;
    }
    
    public void setNivel(int nivel){
        this.nivel=nivel;
    }
    
    public int getNivel(){
        return this.nivel;
    }
    
    
    //METODOS SINGLEPLAYER
    public int getVidasH1(){
        return heroe1.getVidas();
    }
    
    public void setLabelVidasH1(){
        vida.setText("P1 " + heroe1.getVidas());
    }
    
    public boolean getTurnoH1(){
        return heroe1.getTurno();
    }
    
    public void setTurnoH1(boolean turno){
        heroe1.setTurno(turno);
    }

    //METODOS MUTILPLAYER
    public int getVidasH2(){
        return heroe2.getVidas();
    }
    
    public void setLabelVidasH2(){
        vida.setText("P2 " + heroe2.getVidas());
    }    
    
    public void setTurnoH2(boolean turno){
        heroe2.setTurno(turno);
    }

    public boolean getTurnoH2(){
        return heroe2.getTurno();
    }
    
    //FIN PLAYERS
    
    public synchronized void start(){
        corriendo=true;
        thread = new Thread(this);
        thread.start();
    }
    
    public synchronized void stop(){
        corriendo=false;
        try {
            //Cierra el hilo de manera segura
            thread.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    
    public void run(){
        final int ns_por_segundo=1000000000;
        final byte aps_objetivo=60;
        final double ns_por_actualizacion= ns_por_segundo/aps_objetivo;
        
        long referenciaActualizacion = System.nanoTime();
        long referenciaContador = System.nanoTime();
        
        double tiempoTranscurrido;
        double delta=0;
        double timer=0;
        
        requestFocus();
        
        while(corriendo){
            FXPlayer.volume = FXPlayer.Volume.LOW;
            if(!FXPlayer.LEVEL.isRunning() && !FXPlayer.START.isRunning()){
            FXPlayer.LEVEL.play();
            }

            final long inicio_bucle = System.nanoTime();
            
            tiempoTranscurrido = inicio_bucle - referenciaActualizacion;
            
            referenciaActualizacion = inicio_bucle;
            
            delta +=tiempoTranscurrido / ns_por_actualizacion ;
            
            timer +=tiempoTranscurrido / ns_por_segundo ;
            
            while (delta>=1){
                actualizar();
                delta --;
                mostrar();
            }
            
            while(timer>=1 && mapa.getNivel()!=3){
                timer --;
                temporizador --;
                tiempo.setText("TIME " +temporizador);
                if(temporizador==0){
                    if(multiplayer){
                        if(heroe1.getTurno()){
                            heroe1.matarHeroe();
                        }
                        else if(heroe2.getTurno()){
                            heroe2.matarHeroe();
                        }
                    }
                    else{
                        heroe1.matarHeroe();
                    }
                    
                    
                }   
            }
            if(mapa.getNivel()==3){
                temporizador=200;
                tiempo.setText("TIME " +temporizador);
            }
        }
    }
}
