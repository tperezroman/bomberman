package bomberman;

import bomberman.objetos.Bomba;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;
import sprite.Sprite;
import bomberman.objetos.Llama;
import java.util.ArrayList;
import java.util.List;
import bomberman.entes.Fantasma_Celeste;
import bomberman.entes.Fantasma_Rosa;
import bomberman.objetos.Puerta;


public class Mapa {
    protected int ancho;
    protected int alto;
    public Tile[] tilesCatalogo;//arreglo de tiles
    private int[] pixeles;
    private Random random;
    private int posicion;
    private int contador;
    public  List<Fantasma_Rosa> creep_rosa;
    public  List<Fantasma_Celeste> creep_celeste;
    public  List<Llama> llama;
    public  List<Bomba> bomba;
    public  List<Integer> posBomba;
    public  List<Integer> bloque;
    private int nivel=1;
    private Puerta puerta;
    private Bomberman bomberman;

    public Mapa(String ruta,Bomberman bomberman){
        creep_rosa = new ArrayList<Fantasma_Rosa>();
        creep_celeste = new ArrayList<Fantasma_Celeste>();
        bomba = new ArrayList<Bomba>();
        posBomba = new ArrayList<Integer>();
        llama = new ArrayList<Llama>();
        bloque = new ArrayList<Integer>();
        random = new Random();
        cargarMapa(ruta);
        generarMapa();
        this.contador=0;
        puerta = new Puerta(true,this);
        this.bomberman=bomberman;
    }
    public void generarMapa() {
        for(int i=0 ; i<pixeles.length; i++){
            switch (pixeles[i]){
                case 0xff6d9363:  tilesCatalogo[i] = Tile.SUELO;    continue;
                case 0xff696969:  tilesCatalogo[i] = Tile.PARED_SOLIDA; continue;
                case 0xffcccccc:  tilesCatalogo[i] = Tile.PARED_ROMPIBLE; continue;
                default:          System.out.println("default");  tilesCatalogo[i] = Tile.VACIO;
            }
        }
    }

    public void cargarMapa(String ruta) {
        try { 
            BufferedImage imagen = ImageIO.read(Mapa.class.getResource(ruta));
         
            ancho = imagen.getWidth();
            alto = imagen.getHeight();

            //inicializo el arreglo de tiles del catalago y pixeles
            tilesCatalogo = new Tile[ancho * alto];
            //Dentro de pixeles guardamos el valor que tiene cada cuadrado
            pixeles = new int[ancho * alto];
            
            imagen.getRGB(0, 0, ancho, alto, pixeles, 0, ancho);
        } catch (IOException ex) {
            System.out.println("Error al cargar imagen en clase MapaCargado");
            ex.printStackTrace();
        }
    }
    
    public void actualizar(){
    }
    
    public void mostrar(final int compensacionX, final int compensacionY, Pantalla pantalla){
        pantalla.estableceDiferencia(compensacionX, compensacionY);
        //puntos cardinales, para representar bordes del Mapa
        int e = (compensacionX + pantalla.obtenAncho() + Tile.LADO) >> 5;//bit shifting(4 veces mas rapido que una division normal) para optimizar el rendimiento
        int o = compensacionX >> 5;
        int n = compensacionY >> 5;
        int s = (compensacionY + pantalla.obtenAlto() + Tile.LADO) >> 5;
        contador ++;
        for(int y=n; y<s ; y++){
            for(int x=o; x<e; x++){
                if(x<0 || y<0 || x>= ancho || y>=alto){
                    Tile.VACIO.mostrar(x, y, pantalla);
                }
                else{
                    tilesCatalogo[x +y * ancho].mostrar(x, y, pantalla);
                }
            }
        }
        
        for (int i = 0; i < creep_rosa.size(); i++) {       
            creep_rosa.get(i).actualizar(pantalla);
            if (creep_rosa.get(i).isDelete())
		creep_rosa.remove(i);
	}
        
        for (int i = 0; i < creep_celeste.size(); i++) {       
            creep_celeste.get(i).actualizar(pantalla);
            if (creep_celeste.get(i).isDelete())
		creep_celeste.remove(i);
	}
        
        for (int i = 0; i < bomba.size(); i++) {
            bomba.get(i).actualizar(pantalla);
	}
        
        for (int i = 0; i < llama.size(); i++) {
            llama.get(i).actualizar(pantalla);
	}
    }
    
    
    
    
    public void addFantasmaRosa(int creeps) {
		for (int i = 0; i < creeps; i++) {
                        int x = getRandomX();
			int y = getRandomY();

			while (getTilesCatalogo(x + y * ancho).isSolid()) {
				x = getRandomX();
				y = getRandomY();
			}
                        
                    this.creep_rosa.add(new Fantasma_Rosa(this,x<<5,y<<5, Sprite.FANTASMA1_MOVIMIENTO1_SPRITE));
		}
    }
    
    public void addFantasmaCeleste(int creeps) {
		for (int i = 0; i < creeps; i++) {
                        int x = getRandomX();
			int y = getRandomY();

			while (getTilesCatalogo(x + y * ancho).isSolid()) {
				x = getRandomX();
				y = getRandomY();
			}
                        
                    this.creep_celeste.add(new Fantasma_Celeste(this,x<<5,y<<5, Sprite.FANTASMA1_MOVIMIENTO1_SPRITE));
		}
    }
    
    public void addFantasmaColision(int fantasma,int x,int y){
        for(int i=0; i<fantasma; i++){
            this.creep_celeste.add(new Fantasma_Celeste(this,x,y, Sprite.FANTASMA2_MOVIMIENTO1_SPRITE));
        }
    }
    
    public void addBomba(Bomba bomba) {
		this.bomba.add(bomba);	
    }
    
    
    public void addLlama(Llama llama) {
		this.llama.add(llama);
    }

    private int getRandomX() {
        int num = random.nextInt(21)+4;
	return num;
	}

    private int getRandomY() {
	int num = random.nextInt(10)+4;
	return num;
    }
    
    public Tile getTilesCatalogo(int posicion){
        return tilesCatalogo[posicion];//nos devuelve el cuadro que queremos
    }
    
    public int getAncho(){
        return this.ancho;
    }
    
    public void addPuerta(int posicion){
        this.puerta.addPuerta(posicion);
    }
    
    public void setPuerta(boolean estado){
        this.puerta.setPuerta(estado);
    }
    
    public boolean getPuerta(){
        return this.puerta.getPuerta();
    }
    
    public int getNivel(){
        return this.nivel;
    }
    public void setNivel(int nivel){
        this.nivel = nivel;
    }
    
    public int getTemporizador(){
        return bomberman.getTemporizador();
    }
    
    public void setPuntos(int puntos){
        bomberman.setPuntos(puntos);
    }
}
