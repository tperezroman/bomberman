package bomberman;

import bomberman.objetos.Bomba;
import bomberman.entes.Fantasma;
import bomberman.entes.Heroe;
import bomberman.objetos.Llama;


public class Pantalla {
    public final int width;
    public final int height;
    public final int[] pixels;
    private int diferenciaX;
    private int diferenciaY;
	
    public Pantalla(int width, int height) {
            this.width = width;
            this.height = height;
            pixels = new int[width * height];

    }
        
    //x e y definen el punto donde estamos, y las compensaciones lo que nos movimos de ese punto
    public void MostrarTile(int compensacionX, int compensacionY,Tile tile){
        compensacionX -= diferenciaX;
        compensacionY -= diferenciaY;
        for(int y=0; y<tile.sprite.obtenerAncho(); y++){
            int posicionY = y + compensacionY;
            for(int x=0; x<tile.sprite.obtenerAncho(); x++){
                int posicionX = x + compensacionX;
                //Este if es para no dibujar de mas dentro de la ventana y consumir menos ram
                if(posicionX < -tile.sprite.obtenerAncho() || posicionX>=width || posicionY<0 || posicionY>=height){
                    break;
                }
                if(posicionX<0){
                    posicionX = 0;
                }
                pixels[posicionX + posicionY * width] = tile.sprite.pixeles[x + y * tile.sprite.obtenerAncho()];
            }
        }
    }
        
    public void mostrarHeroe(int compensacionX, int compensacionY,Heroe heroe){
        compensacionX -= diferenciaX;
        compensacionY -= diferenciaY;
        for(int y=0; y<heroe.getSprite().obtenerAncho(); y++){
            int posicionY = y + compensacionY;
            for(int x=0; x<heroe.getSprite().obtenerAncho(); x++){
                int posicionX = x + compensacionX;
                if(posicionX < -heroe.getSprite().obtenerAncho() || posicionX>=width || posicionY<0 || posicionY>=height){
                    break;
                }
                if(posicionX<0){
                    posicionX = 0;
                }
                int colorPixelHeroe = heroe.getSprite().pixeles[x + y * heroe.getSprite().obtenerAncho()];
                if(colorPixelHeroe != 0xffff00ff){//solo lo guarda en pixeles si es diferente del rosa que pusimos de fondo en el png
                    pixels[posicionX + posicionY * width] = colorPixelHeroe;
                }
            }
        }
    }
    
    public void mostrarFantasmas(int compensacionX, int compensacionY,Fantasma fantasma){
        compensacionX -= diferenciaX;
        compensacionY -= diferenciaY;
        for(int y=0; y<fantasma.getSprite().obtenerAncho(); y++){
            int posicionY = y + compensacionY;
            for(int x=0; x<fantasma.getSprite().obtenerAncho(); x++){
                int posicionX = x + compensacionX;
                if(posicionX < -fantasma.getSprite().obtenerAncho() || posicionX>=width || posicionY<0 || posicionY>=height){
                    break;
                }
                if(posicionX<0){
                    posicionX = 0;
                }
                int colorPixelHeroe = fantasma.getSprite().pixeles[x + y * fantasma.getSprite().obtenerAncho()];
                if(colorPixelHeroe != 0xffff00ff){//solo lo guarda en pixeles si es diferente del rosa que pusimos de fondo en el png
                    pixels[posicionX + posicionY * width] = colorPixelHeroe;
                }
            }
        }
    }
    
    public void mostrarBomba(int compensacionX, int compensacionY, Bomba bomba) {
        compensacionX -= diferenciaX;
        compensacionY -= diferenciaY;
        for(int y = 0; y < bomba.getSprite().obtenerAncho(); y++) {
                int ya = compensacionY + y;
                for(int x = 0; x < bomba.getSprite().obtenerAncho(); x++) {
                    int xa = compensacionX + x;
                    if(xa<0){
                        xa = 0;
                    }
                    if(ya<0){
                        ya = 0;
                    }
                    int color = bomba.getSprite().pixeles[x + y * bomba.getSprite().obtenerAncho()];
                    if(color != 0xffff00ff){
                        pixels[xa + ya * width] = color;
                    }
                }
        }
    }

    public void mostrarLlama(int compensacionX, int compensacionY, Llama llama) {
        compensacionX -= diferenciaX;
        compensacionY -= diferenciaY;
        for(int y = 0; y < llama.getSprite().obtenerAncho(); y++) {
                int ya = compensacionY + y;
                for(int x = 0; x < llama.getSprite().obtenerAncho(); x++) {
                    int xa = compensacionX + x;
                    if(xa<0){
                        xa = 0;
                    }
                    if(ya<0){
                        ya = 0;
                    }
                    int color =  llama.getSprite().pixeles[x + y * llama.getSprite().obtenerAncho()];
                    if(color != 0xffff00ff){
                        pixels[xa + ya * width] = color;
                    }
                }
        }
    }

    public void estableceDiferencia(final int diferenciaX, final int diferenciaY){
        this.diferenciaX = diferenciaX;
        this.diferenciaY = diferenciaY;
    }

    public int obtenAncho(){
        return width;
    }

    public int obtenAlto(){
        return height;
    }
        
        
        
}
