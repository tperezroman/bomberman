package bomberman;

import sprite.Sprite;

public class Tile {
    public int x;
    public int y;
    public Sprite sprite;
    private boolean solid;
    private boolean breakable;
    public static final int LADO = 32;
    //Coleccion de cuadros o tiles
    public static final Tile VACIO = new Tile(Sprite.VACIO);
    public static final Tile PARED_SOLIDA = new Tile(Sprite.PAREDSOLIDA_SPRITE, true, false);//uso el otro constructor para que sea solido
    public static final Tile SUELO = new Tile(Sprite.SUELO_SPRITE);
    public static final Tile SUELO_SOLIDO = new Tile(Sprite.SUELO_SPRITE,true,false);
    
    //Coleccion de pared rompible
    public static final Tile PARED_ROMPIBLE = new Tile(Sprite.PAREDROMPIBLE0_SPRITE, true, true);
    public static final Tile PARED_ROMPIBLE1 = new Tile(Sprite.PAREDROMPIBLE1_SPRITE, false, false);
    public static final Tile PARED_ROMPIBLE2 = new Tile(Sprite.PAREDROMPIBLE2_SPRITE, false, false);
    public static final Tile PARED_ROMPIBLE3 = new Tile(Sprite.PAREDROMPIBLE3_SPRITE, false, false);
    public static final Tile PARED_ROMPIBLE4 = new Tile(Sprite.PAREDROMPIBLE4_SPRITE, false, false);
    public static final Tile PARED_ROMPIBLE5 = new Tile(Sprite.PAREDROMPIBLE5_SPRITE, false, false);
    public static final Tile PARED_ROMPIBLE6 = new Tile(Sprite.PAREDROMPIBLE6_SPRITE, false, false);
    //Coleccion de bonus
    public static final Tile BONUS_BOMBA_EXTRA = new Tile(Sprite.BONUS_BOMBA_EXTRA, false, true);
    public static final Tile BONUS_DETONADOR = new Tile(Sprite.BONUS_DETONADOR, false, true);
    public static final Tile BONUS_RANGO = new Tile(Sprite.BONUS_RANGO_EXPLOSION, false, true);
    public static final Tile BONUS_SALTO = new Tile(Sprite.BONUS_SALTO_BOMBA, false, true);
    public static final Tile BONUS_VELOCIDAD = new Tile(Sprite.BONUS_VELOCIDAD, false, true);
    public static final Tile BONUS_VIDA = new Tile(Sprite.BONUS_VIDA, false, true);
    
    public static final Tile PUERTA = new Tile(Sprite.PUERTA_SPRITE, false, true);
    
    public Tile(Sprite sprite){
        this.sprite=sprite;
        solid = false;
    }
    public Tile (Sprite sprite, boolean solid, boolean breakable){
        this.sprite = sprite;
        this.solid = solid;
        this.breakable = breakable;
    }
    
    public void mostrar(int x, int y, Pantalla pantalla){
        pantalla.MostrarTile(x << 5, y << 5, this);
    }
    
    public boolean isSolid(){
        return solid;
    }
    
    public void setSolid(boolean estado){
        this.solid=estado;
    }
    
    public boolean isBreakable(){
        return breakable;
    }
}
