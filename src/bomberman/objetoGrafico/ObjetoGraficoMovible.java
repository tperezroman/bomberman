package bomberman.objetoGrafico;

public abstract class ObjetoGraficoMovible extends ObjetoGrafico{
    
    protected char direccion = 'n';
    protected boolean isMoving = false;
    public void actualizar(){}
    public void mostrar(){}
    
    public abstract void mover(int dx, int dy);
}