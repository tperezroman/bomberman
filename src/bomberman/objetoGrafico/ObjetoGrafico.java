package bomberman.objetoGrafico;

import bomberman.Mapa;
import sprite.Sprite;


public abstract class ObjetoGrafico {
    //posicion en el Mapa del objeto grafico
    private int x;
    private int y;
    protected Sprite sprite;

    private boolean eliminado = false;
    protected Mapa mapa;
    
    public void actualizar(){}
    public void mostrar(){
    }
    
    public void eliminar(){
        this.eliminado = true;
    }
    
    public int getX(){
        return this.x;
    }
    public void setX(int x){
        this.x = x;
    }
    public void moverX(int dx){
        this.x += dx;
    }
    public int getY(){
        return this.y;
    }
    public void setY(int y){
        this.y = y;
    }
    public void moverY(int dy){
        this.y += dy;
    }
    public boolean isDelete(){
        return this.eliminado;
    }
    
    public Sprite getSprite(){
        return sprite;
    }
    
    public void setSprite(Sprite sprite){
        this.sprite = sprite;
    }
    
    public abstract boolean  colision(int dx, int dy);//para detectar colisiones
}
